// auteur : Marc Gauthier
// licence : http://creativecommons.org/licenses/by-nc-sa/2.0/fr/ licence Creative Commons Paternité-Pas d'Utilisation Commerciale-Partage des Conditions Initiales à l'Identique 2.0 France
//
//
// quelques variables globales
//
var mga;
var conf = {
  lon: -1.61,
  lat: 48.115,
  zoom: 12,
};

var map; // L.map
var lcontrol; // L.control.layers
//
// la fonction d'initialisation de la carte
function map_init() {
  console.log("map_init() dÃ©but");
// pour les couches IGN GÃ©oPortail
  function layerUrl(layer) {
    return "http://wxs.ign.fr/"
      + conf.apiKEY + "/geoportail/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&"
      + "LAYER=" + layer
      + "&STYLE=normal&TILEMATRIXSET=PM&"
      + "TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=image%2Fjpeg";
  }
  var ign_cartes = new L.tileLayer(
// GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD
    layerUrl("GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD"), {
      attribution: "&copy; <a href='http://www.ign.fr'>IGN</a>"
    }
  )
  var ign_photos = new L.tileLayer(
    layerUrl("ORTHOIMAGERY.ORTHOPHOTOS"), {
      attribution: "&copy; <a href='http://www.ign.fr'>IGN</a>"
    }
  )
//
// couche "osmfr"
  var osm_fr = L.tileLayer('http://tilecache.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
    attribution: 'donn&eacute;es &copy; <a href="http://osm.org/copyright">OpenStreetMap</a>/ODbL - rendu cquest',
    minZoom: 1,
    maxZoom: 20
  });
// la couche par dÃ©faut
  map = L.map('map', {
    layers: [ign_cartes],
//    maxBounds: [[48.071,-1.763],[48.158, -1.614]],
    editInOSMControlOptions: {
      position: 'topleft'
    },
    continuousWorld: true,
    worldCopyJump: false
  }).setView([conf.lat, conf.lon], conf.zoom);

// les couches de base
  var baseLayers = {
    "IGN cartes": ign_cartes,
    "IGN photos": ign_photos,
    "OSM France": osm_fr,
  };

  var overlayLayers = {
//    "ligne": ligne_layer,
//    "ways 20m": ligne_way_distances,
//    "OSM" : osm_layer
  };
  controlLayers = L.control.layers(baseLayers, overlayLayers, {
    collapsed: false
  }).addTo(map);
//
// les coordonnÃ©es de la souris
  L.control.mousePosition( { position: 'bottomright' } ).addTo(map);
}
//
// un marqueur pour la position
function map_position() {
  // la position en fonction d'un clic ou par dÃ©placement du marqueur
  map.on('click', onMapClick);
  var markerLayer;

  function display_latlng(ll) {
    window['latitude'].innerHTML = ll.lat.toFixed(6);
    window['longitude'].innerHTML = ll.lng.toFixed(6);
  }
  var center = map.getCenter();
  display_latlng(center);
  function onMapClick(e) {
    display_latlng(e.latlng);
    if (markerLayer) {
      map.removeLayer(markerLayer);
    }
    markerLayer = new L.marker(e.latlng, {
      draggable: 'true',
    });
    markerLayer.on('dragend', function(event) {
      var marker = event.target;
      display_latlng(marker.getLatLng());
    });
    map.addLayer(markerLayer);
  };
}
function cases_init() {
  $('td').click(function() {
    var $cell = $(this);
    $('td.activecell').removeClass('activecell');
    $cell.addClass('activecell');
    $cell.children('input').attr('checked', 'checked');
  });
}
//
// la page est prÃªte,
window.onload = function() {
  console.log("window.onload()")
// pour l'api GÃ©oportail
  var apiKEYs = {
    'localhost': '9m2r220489kkm1x2s7lq6wdh',
    'bv': '3os2g9rimugnfxc2nlym5xce',
    'www.bretagne-vivante.org': 'uit9cf7rn73kf8qevrgjemfd',
    'www.bretagne-vivante-dev.org': 'uu5fmm020wh4jcm7blyrsn0u',
    'bretagne-vivante-dev.org': 'uu5fmm020wh4jcm7blyrsn0u',
    'ao35.free.fr': '61y24ij6lwe2wpro30f2ge45',
    'atlasnw.free.fr': 'dgdc5nlbj5apwtmuk4wp4ecn',
    'mga.alwaysdata.net': 'wv7w208rgqxf81bp4chcfmx0'
  };
  conf.apiKEY = apiKEYs[window.location.host];
  map_init();
  map_position();
  cases_init();
  var infoDiv = document.getElementById("info");
  var html = "<p>Leaflet version " + L.version + "</p>";
  html += "<p>Extension Leaflet version " + Gp.leafletExtVersion + " (" + Gp.leafletExtDate + ")</p>";
  infoDiv.innerHTML = html;
}
//
// quelques variables globales
//

var mga;
var conf = {
  lon: -1.61,
  lat: 48.115,
  zoom: 12,
};

var map; // L.map
var controlLayers; // L.control.layers
//
// la fonction d'initialisation de la carte
function map_init() {
  console.log("map_init() début");
// pour les couches IGN GéoPortail
  function layerUrl(layer) {
    return "http://wxs.ign.fr/"
      + conf.apiKEY + "/geoportail/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&"
      + "LAYER=" + layer
      + "&STYLE=normal&TILEMATRIXSET=PM&"
      + "TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=image%2Fjpeg";
  }
  var ign_cartes = new L.tileLayer(
// GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD
    layerUrl("GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD"), {
      attribution: "&copy; <a href='http://www.ign.fr'>IGN</a>"
    }
  )
  var ign_photos = new L.tileLayer(
    layerUrl("ORTHOIMAGERY.ORTHOPHOTOS"), {
      attribution: "&copy; <a href='http://www.ign.fr'>IGN</a>"
    }
  )
//
// couche "osmfr"
  var osm_fr = L.tileLayer('http://tilecache.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
    attribution: 'donn&eacute;es &copy; <a href="http://osm.org/copyright">OpenStreetMap</a>/ODbL - rendu cquest',
    minZoom: 1,
    maxZoom: 20
  });
// la couche par défaut
  map = L.map('map', {
    layers: [ign_cartes],
//    maxBounds: [[48.071,-1.763],[48.158, -1.614]],
    editInOSMControlOptions: {
      position: 'topleft'
    },
    continuousWorld: true,
    worldCopyJump: false
  }).setView([conf.lat, conf.lon], conf.zoom);

// les couches de base
  var baseLayers = {
    "IGN cartes": ign_cartes,
    "IGN photos": ign_photos,
    "OSM France": osm_fr,
  };

  var overlayLayers = {
//    "ligne": ligne_layer,
//    "ways 20m": ligne_way_distances,
//    "OSM" : osm_layer
  };
  controlLayers = L.control.layers(baseLayers, overlayLayers, {
    collapsed: false
  }).addTo(map);
//
// les coordonnées de la souris
  L.control.mousePosition( { position: 'bottomright' } ).addTo(map);
  L.control.navbar().addTo(map);
}
//
// un marqueur pour la position
function map_position(actif=true) {
  console.log("map_position() ", actif)
  if ( actif == false ) {
    console.log("map_position() off")
    map.off('click', onMapClick);
    return;
  }
  // la position en fonction d'un clic ou par déplacement du marqueur
  map.on('click', onMapClick);
  var markerLayer;

  function display_latlng(ll) {
    $('#latitude').val(ll.lat.toFixed(6));
    $('#longitude').val(ll.lng.toFixed(6));
  }
  var center = map.getCenter();
  display_latlng(center);
  function onMapClick(e) {
    display_latlng(e.latlng);
    if (markerLayer) {
      map.removeLayer(markerLayer);
    }
    markerLayer = new L.marker(e.latlng, {
      draggable: 'true',
      icon: L.AwesomeMarkers.icon({icon: 'star', prefix: 'glyphicon', markerColor: 'yellow', iconColor: 'black'})
    });
    markerLayer.on('dragend', function(event) {
      var marker = event.target;
      display_latlng(marker.getLatLng());
    });
    map.addLayer(markerLayer);
  };
}
//
// la couche avec les observations
var obs_layer;
function obs_init() {
  console.log("obs_init()");
//
// icone avec couleur personnalisable
  function getIcone(couleur) {
    var images = {
      black : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABX1BMVEUAAAAsLCwLCwsYGBgLCwsaGhobGxsVFRUSEhILCwsICAgLCwsLCwsMDAxDQ0MsLCwyMjIuLi4mJiYfHx8VFRUKCgoJCQkJCQkICAgKCgoqKioqKioTExMoKCgkJCQQEBA0NDQWFhYKCgoZGRktLS0JCQkVFRUaGhorKysTExMbGxsICAgHBwcWFhYoKCgJCQkaGhoXFxcICAhBQUE9PT06OjolJSUqKioqKiojIyMjIyMsLCwHBwcJCQkrKysKCgoiIiIKCgolJSX///8jIyMoKChFRUVNTU1ISEgtLS0eHh4ZGRlLS0s4ODgxMTE2NjY7OzszMzNAQEA9PT0vLy9CQkJcXFxKSkpPT08rKysSEhJgYGBTU1NaWlpWVlZiYmJSUlIgICD6+vpYWFivr6+GhoZmZmY/Pz/f39/Z2dnMzMy7u7uioqKampqXl5eNjY16enp3d3dnZ2dXV1cLCws510p7AAAAQ3RSTlMA/vUE+PUTCfbMdksB/Pr69vb29vbw59+EUi4lDvj29vTx6+fm1sCvhYJxaFtYREI8Ghf4+Pjw0sy+tI+Me3huZWJR2lRSigAAAhFJREFUOMt10mdzolAUBuATorGmqrGk97pp23tRuHQuINWuKdv7/5/lIo64mucL5513ztwBLgzNlM5KMzBm5XDXWdxcdHZfliAs/WbxYbt2+/u21ZybP5wOLTxbaLTMZttuN82aufl0BQJnS3OtHhrotebWPgQnP7Frtjhkm/WlSyBePDZFlrCQ1X+aj56T4sFSA5GMGjWz1rD90Vx7BxDLoDrjqTd0p0PjZtMPKBODi3WTzLZN//rU/XhDozaJ5voFHC/UDcNgevK3MnHt/GG9XF84hgMdaZomMnq5D2kkI/0AdlhG13VR+hk0P1Tbywy7A1uWgTEWlZug+a4iLxvWFuxjRpIkRheDpqawJON9OJoXvUlH/Be/+CxYmpfF+SNIRZHiYbDwtVvuXvMSSyKKpuBy1pJUVVVYjXerruCd6CVJnL0CSMiGTGiIwQbS/NlQE+S7ZS25SsgKVoLJihe9ZjqHMTcKS8v+fy1kmKkRHBNNAnEVkRQ6TFEiwR16FdVGGi1aGFy0iKpWhlSVWoVAIXsXau6yr2FglZLl4Uo1R1YCyTju8H0VHE/CUCzncEHDOdsxCHkbl3iB4KWNEwiLbdOO30zR5PXDTjYUv1Go9zBqerlCu65LV/JkZcQpJQuuIFOn8L90vlNxK518GsYUKU7gqCKMS+/xHL8Hk6QidCQFEyVmEzDZ+d9zuEcyHP4BydyrWwgpGWwAAAAASUVORK5CYII=',
      blue : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABTVBMVEUAAAATE/8ZGf8YGP8jI/8jI/9FRf8rK/8cHP8gIP8SEv8SEv8XF/8REf8VFf8wMP81Nf8kJP8QEP8REf8QEP8SEv8uLv8+Pv8yMv8pKf8VFf83N/8REf8cHP8SEv8fH/8REf8xMf8REf8REf8uLv8REf8VFf8cHP8gIP8wMP8aGv8PD/8hIf8REf8bG/8REf8gIP8QEP8MDP8rK/8vL/8qKv8qKv8REf8oKP8oKP8QEP8uLv8vL/8nJ/8qKv8WFv8qKv8wMP8xMf8uLv8sLP8wMP////9NTf9KSv89Pf8qKv81Nf86Ov8nJ/8sLP9PT/9DQ/8fH/8zM/8jI/9HR/8/P/9GRv83N/9bW/9WVv9eXv9ZWf9TU/9hYf/7+/8aGv9mZv8YGP+urv+YmP+Fhf9jY//e3v/Y2P/Ly/+6uv+hof+MjP95ef92dv8REf+YOAJyAAAARXRSTlMA+AP2E/769/b171MZB/359vaEeWJKLvj29vb09PHq5+fm3dbPzsnAr4yCdHFsWkI8DgsK+vTw4r60jIJ4ZVFOR0InJyKsMvUiAAACCElEQVQ4y3XSVZviMBQG4NMyOIwC4+5u624lCQ1NW1qq2Mj67v+/3LSUQYZ9r86XL8+TXBzomyh9LE3AI58ON6WF5QVp87AEg8Q3Uwm/0W63753Ek9div/iyM+01bMdnvmM33OWdzxD5kEw0PNbj3SeS76KXn7EGQ33M9Z/mIPBy3kU4hHCXO/8CuLOkHZ4wz7Vdm4U37ORbgOsZ5Buc45lSXCGOEwQfzVzDxaJd5xhT/nxrfr2rID+I9uIFnEw7qqoann5bDtxIHubZmT6BAxPxCRlmsxxCapjNA1jHhmmaiPwud/2yGM8GXodVXCeEIOsuam5lxnMdr8I+MfhkmChqXIqDTPbhaApRSk1U+xEW3zWs8oymjuB9mlHOINrPZrl5UyM4iCxdhNwkprIsW1itdfSWxl/kiaLJHMCerMoB/nNSR9Es7wFANoVlPUQJ7U4yjmV5k88QIg0jdE0EbnamPtIY6WMI5ARiKYMoFaIdepVWlUqfoqZme4smWPJAY8krlxCZTZmVak/FTM1Bz+WKrj80sp65ggdzMVKtdVVJ7Bj6rjKSHjW6lMnDgNMYrWmBGo2dwqD8miKFjaRs5GFIIWaFjbVUgGHiRlVptVpK9bkIIwpLutbRdOEMRolb1UqnEt8S4ZGsIGmSkIUxtuNSfBvGKQqKUISxdid3Ybzzv+fwH3OD4R9zIK/7H5PSoAAAAABJRU5ErkJggg==',
      cyan : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABKVBMVEUAAAAa//8U//8b//82//8i//8j//8U//9F//8S//8T//8T//8Q//8a//8b//8w//8r//8n//8d//8S//8i//8T//8c//8u//8W//8+//8x//8R//8q//8f//8x//8R//8R//8R//8c//8v//8W//8h//8R//8d//8b//8S//8g//8u//8d//8Q//8Q//8s//8u//8u//8o//8w//8Q//8v//8Q//8Q//8q//8v//8M//8r//////89//9N//81//9K//86//9G//8n//8j//8v//9P//8x//8z//9I//9d//9B//9D//8e//8h//9W//83//9a//9i//9T//8Z///7//9m//+u//+Y//+F//8T///e///Y///L//+6//+h//+M//95//92///vg/FoAAAAO3RSTlMA9vcC9fYT+/rvzIR2UQn6+Pby6bFLQiUG+Pfz8ufm5N3WwIV9cWxjWlU8LhoXDfbSzL6PjHhmXUctDyP+IMEAAAIBSURBVDjLddIHk6IwGAbgD9C1rlss6/bey/WKCQnSDCjFsuX6/f8fcQniiqv3zDCTl5dvwkwCU0tfGtdLMGfjeM/cymTMvXfXkKZ82Mr3o9FoFPn5lWMlNfAmE0bM71t9n0Us83oDEp9X81FoTYRRfnU52XnHiiw8ZbH+zg0ItU2GSQyTMbb5FriL1RB3OCtkjIWWWGL26hygsob7OueHrmmoyPc7PPRxtgJXa0wUlqX+/T789qiOv2NrV3Ca8T3P00PnoSXcm36HZz9zCjWEXdfFOhq2YtiNM6rBLukghDD60xr7HVg8d8gurBNdNMFj0jxootHJOhwhPsMfnDSMkjgfQWkFU0pdbPyMix894vGMV05gOWtRroO6v4at4b2BiIjWeh1ucoRqmhYQ1xg4gx7fkSeKc7cARc3TBPHnOo7X/E0RAC4lojkxiuh4pRFJnEOzgJA9C9GqAlwpq5szbD1bBuE2hwI1jVI5uUPvs95M40mlyUWTAy1VBNr2V0iUJFdtT6iudAITd9uO89xoTuEOnpUl1DbG2kgqw1SlYNpJY5uFCqScSdToCQaVziCtWVXNrmCq1SbM+CgFPV70AvkcZin7bXUwGKjtfQVe+CQ73UHXkS/gJeWAD6nGgQJzLmW7Z8vLME85NGzjEBapy2quDgsVn4qwWOOpAf9RTod/IruZkti2aToAAAAASUVORK5CYII=',
      darkgreen : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABUFBMVEUAAAAqfioedh4YcxgWcRYUcBQgdyAwgjA2hTYcdRwgeCASbxISbxITbxMXchckeyQRbhESbxIQbhARbhEugC4gdyAWcRZFjkU+ij4ygzIVcRUcdRwfdx8RbhERbhERbhERbhEVcRUofSggdyAvgS8adBoPbQ8heCERbhERbhEQbhAtfy0RbhEgeCAvgS8cdRwMawxFj0UTbxNDjUMRbhE4hjgqfSoxgjExgjEugC4ugC4cdRwcdRwwgTAQbRAvgS8neycleyUqfSosfyz///9JkUk1hDUqfipOlE4+ij5GjkZCjEIvgS8sgCwxgjEnfCc4hzhLkks9iT1Nk007iDszgzNcnFwjeiMedh4jeSMheCFUl1RQlVBin2JZm1lXmVdenl4adBoleyX6/Ppmomauzq6YwJiFtYV3rXfe697Y59jL38u61bqhxqGMuYwRbhEDJUAeAAAARHRSTlMA9wP2/vgT+vb29e9TSxn29OmEeS4JBvn49vbx5+Td1s7Jua+FgnRxbGRbREI8Jw8L+/n49/Lw6OTSzMG/j4x4ZVtRIpl5oF4AAAIJSURBVDjLddLFmuJAFIbhkwCNu7S7e4+7k6IqRiqEIAlOy/j97yYFoQndzLvJ+Z9vkU3B1ELuNLcAj1zsvZaXXyzLrz7kwIv/FHxmtYbDYauzGNzjp+Hb21C7ZXYs2+qYLTP05gJcp9xiq40n2q0n3Ff3zy/trl2ZsrsWlwBmZ8mkAkOx+zWXdlj4wpmYbdzuml3THp0m9xkgn8SW6LDaRO4reqcjsIGTeTjnTBZsW7n7Mfh+W8I2myZ3DochS5IksV29KTLXckdwthU6hF2CnYuKpDiGJbYx2YU1QSCEVPQ7t/zRbGcLwhqsUpEV7dYtNwg7W6Sr8I4Iuq4LpOKWrjHe72E/SA3DILj2axR+NqnkbBrch3gEaw6B1H8PioPrhi6wiSNxSPipgRDSBKnmr/aahGrOMqg/AbCNJMRIWCQidm+0DQCxMEXVEYMY4wtRX8wphaiuq7N0I82DI5MU5RmqGDkAJvFU1xQvTXvuvqGPEUkpTSlSODN5aCkNeYqGUpfgyoSlUnmiJIWzMHGZQui+oGr0Cu5lfXq/NlbWfQcwdRWV1caYKkfz4HHkM2pNpmb4jsCrkFbkOiMr6QLMOPZpTSc0tZVjmMWvl5Ver6eU13l44GQF1Xt1FDiBh/iNfslf6m/w8EgsoDbVQAzm2GyojU2YJx5QAnGYa8u/BfOd/T2D/8h6xz/AN7EGbWu66wAAAABJRU5ErkJggg==',
      deeppink : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABJlBMVEUAAAD/K57/KZ3/KJ3/Jpz/Rar/LJ7/NKL/PKX/I5r/JZv/MKD/KZ3/I5r/U7H/Qaj/JJv/NaP/MaH/OqT/JJr/JJv/JJv/I5r/KZ3/I5r/KZ3/JZv/Pqf/I5r/LZ//QKj/Ta7/MaH/Lp//MKD/Qaj/I5r/I5r/I5r/Pqb/I5r/J5z/LZ//MqH/M6H/L5//MaH/MqH/Uq//SKv/OqX/OaT/OaT/QKf/Ipr/Qaj/Pqb/P6f/Qaj/P6f/PKb/////XLT/TK3/RKn/R6v/O6X/WbP/VrL/OKT/NKL/UbD/P6f/Qqj/MaH/Pqb/VLH/Zbn/Tq7/abr/XrX/bLz/Ybf/cb7/LJ7/+/7/tN3/oNT/jsz/gsb/4PH/2+7/z+n/wOL/qNf/lc7/I5orVJ7NAAAAPnRSTlMAA/b+9/b2E/d2U0MZB/r4+Pb29fPv6YR/altKLg4K+vj08efm5N3Wz87JwK9xY088+PLwvrSPjIiCeCcnIid7hO4AAAH1SURBVDjLddLXduIwFAXQa1NCCzWUACG91+kdybLcsdwCKdP//ydGAjOYhOwXn7POussvgoW1frG/Bs98+XhgVV5nrIP3fUiSPldeReEtx8rbn6TF8O0kw0KfRUHE/NDPnHyFWDFXDhmaY7fl3HX85zdBGKCFIIwaJRDe7fh0BsVff+ctcN2cjzQuYCH/SSAi8nNXAIMaikSLmGfpKmZsWlBtADcN3+WCQH38Mfn+oKJAVL9xA5eZyDRNlyn3Q+HOYhrvUeYSLjzEE3W9yXAKmaIj7wL2qeZ5HsKPw5k/JOBdo/uwR12MMSIP8XJPAt5dugfnWONJ81C8hISKjs+hsy2Sh4xf0+GnQ03eaaUD17uIcBp2fk+GkzsDU1HRbg9KKYpFpqaRUsbObMA0VQI4U0xFMJGLXRRn5QwAClmqzBBM4kTlAl/WmxjbyzBuScC1a661xNbqeRBKVUxGSYRU4zf0oW6qCSOz3p4/tDQhiYWQjS2ItbOmqs+pZrYDc1sbivJ/IYo4mctnsW7M6FjOw8KgadnxYlvNdUjYzGLDEQwsb0LSemtkTRdLbYmT5JFMDHFC5CtYJh3q6ng8HumHEjzRlRVn7CjpLjwlHelqSjWOJHimkLYdO12AFY4N2ziGVXrpUbUHK52mTmG14t8ivCCfLP8AedSe/tUvRC8AAAAASUVORK5CYII=',
      dodgerblue : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABL1BMVEUAAAAumf8zmv9Np/82nP8zmv88n/9arv9Eo/8tmP8sl/85nf8ymv8sl/9Ipf87nv8umP8tl/8sl/8/of8ymv8umP82nP8umP9HpP8sl/82nP9VrP9Jpv8/oP8xmf9Cov86nv8umP8tmP82nf85nf9Jpf8tmP9GpP8tl/8wmf82nP86nv9Hpf81nP8tmP8tmP86nv9Ipf9Np/9Cov9Qqf9Dov9Cov9Cov9Ipf8sl/8tl/9Aof9Fo/////9fsP9Np/9Kpv9Do/9Tqv9jsv9GpP9VrP9Qqf9Ipf87nv9Bov9hsv9dr/9arv8+oP9Yrf9vuP9ls/83nf9rt/9otf90u/8ymv9yuf/8/v+43P+k0v+Tyv+Ixf94vf/i8f/c7v/R6P/C4f+s1v+azf95vf8tl/+ZXjEFAAAAPXRSTlMA+AP29vYT+vfodkMZB/r279+Ec1tUUEouDgr49/b29fT08vHn5tbPzsnAr4WCbGQ8J/728vC+tI+Me2UivHgIRwAAAgJJREFUOMt10tWaozAYBuCfytTHpe24u61bE0ggtEiwytj63v81bEjp0M52Xw74Pz7y5CCB1Ey9Up+Bf9xeHJobb7Lm4Yc6jMssbRSi7tPvp25YmL3IpMWX46zX9cLIj0Lxzh7fQqJSLHRDOhJ2C8XVZOe3ft+3Un4/2q7K5v26ZyGJoiFv/R0Iy0VPfvG9vnh8+YdX/AzQ3KSRLUSeobs6CUMZ6GYTbl57tuM4vq8/fht8fdCoL5Ltbd/AUjaKxxDft2J3ZohEjrJLcG5QzrllG4OWRHmcqXEOB8g2DMMij62hX8wX2UYHsI+cuGEPSXOPqcgO2oczwyaE2IaVNH2GZD6Dy1lLTJy6P2TxvY24yNbsJazuUCbYRvBz0BrcuQTFke6sQbWHCMaYIe721KAtdhSJWL0qwCl2cIxT23Aol7Pz6hQAVnIIqzHMCEsmlI/PoTZPiDqJkJI818U5x5yg2nNliFUVwvRxjCkzIH2c47qW0nlucXTRdhnWUgzvNiCxmONaZ0TjuU8w0tjD+LnB6l4DnpVzxE10SL4Mqea8qbaHVH2+BmOu8sSVhUvyVzCuVtJ12Zh6qQYTrvOsHQRBm21dw6RMqaOJRu8sZOCF5S0c9AKsLMNLmQVX62luuiS1ophtU1mBKY5c0z2CadYUXVmDqU56JzBd5U8F/qM8Hv4CQrugICHxsA4AAAAASUVORK5CYII=',
      firebrick : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABPlBMVEUAAAC6OTm4MjK5NTW/R0e6OTm9QEC6Nze4MjK4MjK3MDC5NjbHXl7BTEzCUFC9QUG8PT25Nze5NTW3MTG3MTG3MTG3MDC4MTG4MzO4MjK3Ly/FWFjBTU2+RUXDUlK8PT27Ojq7PT3BTEy3MDC3MDC3MDDASkq6Ojq8Pj7BSkq5NDS8Pz++QkK3MTHASEi3MDC8Pj7ASkrBS0vAS0vGXFy/Rka+RUW+RUXAS0u3Ly/AS0u/RkbASkq/SEj////ASkq/R0fIYGDKZmbJYmLFWFjEVlbBTU2+RETDU1PJZWXHXV3CTk68Pz/GW1u9QUHCUFC7PDzCUVHMbW3OcnLNcHDLamrLaGjPdHTOcXG6ODj+/PzQd3fRe3vnubnalZXWiorPdnb14uLz3d3v0tLqw8Pira3gpqbfpKTcnJy3MDBhZxUTAAAAPnRSTlMAA/j+9/YTCcyEdhn6+fb29vb28+/paldQSg349vb09PHn5uTd1s/Ar4V9cWBfREI8Lyf6+PC+tI+MeFEtIkjkPFoAAAIJSURBVDjLddLneqJAFAbgA2rsNcaS3nvf3gSm4ACCgr2kbN+9/xtYBnXFxLy/zscHzzDPDMwsfb68WYJnbk92pfXNdWn3/Q34CR+Tr9uPo9Fo6CRenQiz4sthqjVsOm3UdpqPzc3DW5i4DCaGLTTVGiaCnyYrv0HdjjbT6baDceDerTU17EF4rLn2FlznwR5SXKjV7XV7HW9sBs8BwmnU5slpEcmSqePw0EbpMFxv9BRd11FHfvg2+Hovo46blN7GNZylHD62jF8V7k5y+ItO6gyOCTJNU1PIoOJBJs+IHMMOVgghGn2ojP1hHTcreAe2sc4bdj9pftvIzTrehiOiUEoVok2aLsNePoJokk8msn54xfcaNt2Mk6dQ2kLMpZDGz0FlcGdRzCPaKkM8gKlt2wybVsDo19wV3US1QBwgUtfrHP9zXTP56D6JAEAxhOuGhxE2nuo4W+LnmSdUnUfpigCuaFpXJT9VycSAi4uUzTWMiUvg+ZAxZR/JzESnF01ktq9h9vIqTERDplydks3QKUytLhvG/8Y2+CdTsRCtWmNVko3BTDgvqbUxVcqHwacQopZXWDRbAL/wiiQ1OFXm2/cr5FjNLWosdwHzhL2q3O/3peqeAE9c5IxGv2GIRXhK2K/KAdnaF+CZoqjWVLEECxxYqnUAi5RFWSzDQpFABBa7+nsFL4j5wz9CKaISNBFW5QAAAABJRU5ErkJggg==',
      gray : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABOFBMVEUAAABubm5oaGhqampkZGRlZWV8fHxubm54eHh1dXVra2tiYmJkZGSFhYVlZWVzc3NiYmJjY2NiYmJkZGRiYmJjY2N2dnZiYmJdXV1mZmaDg4NiYmJ5eXlubm5ycnJsbGxpaWljY2Nra2tiYmJiYmJiYmJlZWVpaWlzc3NsbGx3d3dhYWF2dnZlZWVtbW1iYmJjY2N1dXViYmJsbGx3d3dhYWF0dHSAgIB4eHh4eHh2dnZ2dnZxcXFxcXFzc3N1dXVycnKKior///+AgIB5eXl1dXV7e3uHh4d3d3eMjIxycnKEhIR9fX1zc3NsbGyCgoKUlJSPj4+GhoZubm6RkZGYmJhpaWmWlpaVlZX9/f3Ly8u8vLywsLCnp6ebm5toaGjq6urm5ube3t7T09PCwsK1tbWcnJxiYmJOKMwqAAAAQXRSTlMA/vYD+P71E/r39nZL+vb18+/fg2pSLhgKBvj49vb09PHq5+fWzsnAua+MiX19cV9YREI8Jw4K+Ojk0sxlW1EiG9e+EucAAAH3SURBVDjLdc3leqNAGAXgL8Slkcbr7m7r8mVgYIInxKrre/93sAMkJXTTlz/ncJ6ZgUCydl5Lwn+uDt4pS4kl5e1BDaZFj1ILdu/xz2PPmUsdRYPhy4eE1Rs6NrOdYW+YeH8FY+fpuZ7FJpzeXPrz+OUVNmAYYAO78BVce2+GGDZc2APuU9pCmWPWgH/MjWilPwLcLKPtNtsylbZEHMcruHIDlwWrwzEmPXwffbvX0XarVbiEk4StqqrsiHct163iyLzbiRPYNxlPKJujlgdV5J2Z+7DBf5omkoeW7zdlvMu4AWuoEkJQux8vd5TxruIa7BKZJ9nE8TLQ0O1kF8opN5nY/ekNPwz/jtQx5FaZxsnE+DVqjW67BN3KVnNQjyOhlGqoRuLik8EH3gjG6wA7VKUuE2XSQT+rdAcAzgSkokcjmp8oClW+NEuEiGGErEeBO1zuKCGinMmCq75INGmapi0mwXOYUUOLmimDLxmjVA9QWryGsXLG1NsTuikcw8R1URSfFyoWG/AsK5B2xNcmQhYCjZIidn2iUmrAlIpAIoYrQoQKTGuuS4q3KNJmE0Iqec3o9/uGlj+FsOimLvFF0uej8MJpXjSeDDF2Bi9F59t6XG8HRwLVmGIosSrMsBVRIlswSy4mxXIw03Z8G2a7+HsBr8hOl39VNKInwCKX3wAAAABJRU5ErkJggg==',
      green : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABRFBMVEUAAAATihMZjRkdjh0WixYrlis2nDYZjBlFokUwmDATiRMQiBATihMSiRIkkyQWixYgkCASiRIRiREuly4uly4RiREvly8PiA8ymjIplCkfkB8RiREcjhwfjx8xmDERiBERiBERiREuly4RiBEVihUcjhwgkCAvmC8WixYhkSERiRERiREQiBASihISihIRiBEgkCANhw0/nz89nz0qlSoSiRIolCgolCgwmDAQiBAvly8nkyclkyUqlSoqlSowmDAolCj///9Op041mjVKpUoymTJIpEgqliowmDAnlCcjkiM+oD46nTotly1Fo0U8njxCoUJcrlwekB5QqFAhkCFUqlQ3nDdZrVlXq1dfr18ajRphsWH6/Ppms2au166FwoV3vHdjsWNAoEDe797Y69jL5cu63bqh0KGZzJmWy5aMxowRiBFwxDb/AAAAQXRSTlMA+PYE/vf1Cfr6hHZLAfb29O/oLiUYEw329vbz8efm4t3Wz87JwK+FfXFsZFtVUUI8E/j48Oy+tI+MeGVbUUdCGypB9uQAAAIFSURBVDjLddFnl5pAGIbhF+xd13V7772k98SZAZE2INW2LT35/9/DKEZc3evT3DxnDocDjC1UP1cXYMr84Ya0lFySNt5UIarwLpN2uv1+/95PZw5jkQs7Sa9r+o7l+GbXTO7MQ+hTIt310Ih3n058DN/8wmpbaMxqO4kvwLx+bpIhRIbMZ6/Y8CFhIjFgeW2zbVrsiMzEe4BUETmsHO9BkjXq+4NAxRRcLZt6wLK0u2+9r7cNZLE0l6/gOOkEJ9HDv+rMjeSLQTvJYzhQkGEYRFTqQ8hgjZQDWA8eKgqid+Hyx7WCFsk6rBGdLe5tuPzGKGidrMG+IlJKRQWFS1slg96HbIaoqvqA5B+D4btNjKBJ5gjyZaQGRKX1s1fv3ciUsETlPNTihGKMXWLIcaFjK8QNipJ4DWAP65gxkKjoyMCMjvcA4JwnWGCwqqjhifAXwRKrUCpMourc4L9mi7o0QRDLOWBqHHW1KFXlrmHg7YqhNcY0YyULQ9eciyOLi1cXIZTljUZzpGHwRzCyuIrx/wULFXYllONpUx5qUj4HY6mKJISLIL1MQcQJr8o2I6v8CUSl5jSpxUga+/yo05JrB4Ptlk5hUmyzqXU6Ha25GYNHzkq41Wlh7gweK2w1G/GGvFWAKeecZEvcBUwrbMuSvA2z5DmNy8NMu/FdmO3y7yU8IReNf/09p3W4HD8vAAAAAElFTkSuQmCC',
      icon : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAAB7FBMVEUAAABDbKI0dqc2e6wwcKA+h7gub5oyeakvcJ1HkMMvbpwxc6IubJgua5g4grYvbpk5hLc4f7AtbZktbZkubZgvbZk3gbctbJo1aZYwdKUkbp4webc8grVEjL42gLIxdKQzdqctbJktbJktbJkxdKMzd6g3gbY0eKcubJgubJcwbpo2gLMubJczd6o3g7QtbZg2frA5g7g5g7c4gbUtbJktbJk4gbQvb50vb502fbA2fbA5grgua5c4g7gubJc1fKwva5c1e6stbJY2frA1gLQ5e7P///8thctGl9AlfsowhsxBlM80icwrgssifMlElc82i808kM4pgcongMo/ks9Dk9FAj9A5jc4yh80geslIl9Eeeclbo9VUn9RKmNI6js5EjMBOm9NJltJJmdFGlNE7i88adcg8gK45eKT7/f1Wn9RXn9FLlc83iM4xhM1LlMpfptdZodVYoNVRndNOlsodeMhHj8RCi8Q1gL5Dib22zt6gwNZIlNJQndFLm9FTnM82is41hc5Qmswzg8xQmMtHkcuDrcovgco/icQ8hsM5g8E+hbxHi7syfbtDh7gxe7hChbM1c6Dg6/Lb5/DR3+rB1eKrxdeXudGNtM88js6Qtc1Aisc8h8Runb1ek7lekrZQirQ1e6xJgqoubJdrKY+aAAAARnRSTlMAA/b29vWDE/769/b1dv359/bv6VRKLhgOCgf++vj48efk3dbAr31xbGRPREI8J/Hw6OTSzs3Mysi+tI+MiHtlXVtaUSIbAP5vdAAAAjZJREFUOMt10kV74mAUhuETrGhdqbt33N0YmBYGCgnu7m51dx13/aOTD9IiZe5NznO9iywSKKhqHm+uggumHlytrmXWVl+51wzFsKFazpohEokYljnVQ1hheH6T6TcYF5fsS4tGg595bQoo410cg1EnpsQNnK5n1Jsvb0Tt4gJ7bK3uBSB3O2Ov83TUM9Z5Bw1P6ny6aZLdGPVHjXZ06nx1jwDa6AkrKqv/wP3bfbAcz0WC3gaTdJ+KZNpwb731vvnkTphQ+uiTMMy0KhQKlfFkXoTMuePTZFuZwzBo0ZGXOPXLK8r5kUKtswxCTxg3m8227JYo7+ORiWw83APdYZVarbYdfaGW+RMT2apwNwxYcLlcjlt+Usv3rCPXA8DqQNfngOd9bnjnCSjIdnSwYIxmmiHhltMPXpF3zpN1oDTRmqCFHZCj25HysP+cetI2FPIAuwXg1j7xiqQhbJvpTRuhQUHs3waARt4CWciKcyV/aRYYY+h71qfVL0updxowILHoeNmC0wSAtNR8DUqLBXf41D90n0bMFpESNBbkVfG/aZQFM7v8VqCwaIRSckZJ8B7CmVb+ruZ80exdaoVzAp5TIsuTOBkCKGir3wtRSyhTL4QiIzynTIvInIwRKCZsyGzrkVCmQQglRhlBbTKZ1AYZo1AK6z2Uulwu6WEvBmUec1f1Lv0q9ymUw/qOZ13S4z4MLmjkrmvXuY1QQX97qL0fKmmq2a5pgoqus29AZRN/J+A/BMXxDw1VynteLoDVAAAAAElFTkSuQmCC',
      lime : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABU1BMVEUAAAAa/xoZ/xkT/xMW/xYg/yAr/ys2/zYW/xYg/yAT/xMQ/xAS/xIT/xMg/yAX/xdF/0Uw/zAy/zIk/yQS/xIS/xIX/xcu/y4g/yAa/xoT/xMp/ykR/xEf/x8R/xER/xER/xEV/xUc/xwg/yAv/y8W/xYh/yER/xEd/x0g/yAQ/xBD/0M//z89/z0e/x4q/yo4/zgc/xwq/yod/x0S/xIx/zEx/zER/xES/xIu/y4u/y4o/ygo/ygw/zAQ/xAv/y8Q/xAq/yox/zEu/y4s/ywf/x8M/www/zD///80/zRJ/0lM/0w9/z0q/yo6/zon/yct/y1G/0Y2/zZP/08i/yJC/0Ik/yRd/11O/04//z8e/x5E/0RU/1Ra/1pX/1dh/2FR/1Ea/xr7//tm/2au/66Y/5iF/4Vj/2Pe/97Y/9jL/8u6/7qh/6GM/4x5/3l2/3YR/xGZvuuzAAAAR3RSTlMA9gP3/hP39vb1hHZTS0MZ+vr29vDpWy4JBvn29Ofk3c7JwK+FfXFsYzwN+Pj49vTy8fDw7ejk19XSzL60j4x4ZlEnJyIQCgQ7zToAAAIJSURBVDjLddJVm6MwGIbhD9qpy0h13G3d3SWFpCkNhUCB6sj67v8/WtLSqUznPuF7r+eAk8DYQjFTXIBrPu4/Rcv3l9GTN0WYJL9bvuW2+/3+ZWtxaV8eh88vo16bt1zdbfE2j774BIFMZLHt6SPe5WLkQ/Dnh3q7g8c63L2XBeH1HY5VAVvBl99+JUIh4lli6x7n3OuI0+KRY4DSiuXWfa5nI1MjrZYqhrVSgpM1LoLe0f5+7327qFodMfnaCRxGXcdx6p5yXhHOUEv1txs9hD3bMgwD1+1eZcAyxLbsPdhQVdu2MflTGfrNdH+r6gY8wA4hBLOLoJwz3d8OfgS7RPUv1cZB4XSwyS4kljCl1LbMn4Pwo4ENf+OlA0jHdepTSfNXr9I7axBVTD2ehmwYU+ZTDTOsdBsEi0FxOAuwoziKYFh14liGIjhsBwBSElaGKKHBhaWUX8o5QtA0QvMy+BIr9ZlSjydByK4Spk2iNBS8obdxQ6uOacbdxOihhRibKIytf4FAQjKqtZGqIR3AyOm6olwVpuRO4UpSIjVzqEakJIx9zSHUGELocQkmHEnUHASTxo5gUjmvoaaAtHwZpryPsYYfGix2DNPkzZrW7Xa12qYMMwoxpdltKqECzJK3atVw1dyS4ZpUCDVRKAVzPDOR+RzmSYe01TTMtR3ehvky/zJwg+Tk+A/29bRZysHv9QAAAABJRU5ErkJggg==',
      navy : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABR1BMVEUAAAAbG40YGIwgIJAUFIo1NZsaGo1FRaIwMJgrK5YcHI4gIJAREYkTE4kQEIgTE4oQEIgkJJMpKZQSEokSEokiIpEQEIgYGIwSEokuLpcREYkTE4oVFYs3N5wcHI4fH48xMZgREYgREYgREYkcHI4vL5gaGo0hIZEQEIgQEIgREYkREYggIJAvL5c/P589PZ8qKpUSEokuLpcuLpcoKJQwMJgQEIgvL5cnJ5MqKpUqKpUwMJguLpcoKJT///8wMJksLJZNTacpKZU1NZtKSqU+PqAjI5JGRqMzM5o4OJxISKQ8PJ47O50mJpNPT6dCQqFcXK5ERKJRUaghIZBWVqtZWa0vL5diYrFTU6ofH5AeHo9eXq8aGo1AQKEXF4z6+vxmZrOurteYmMx3d7ze3u/Y2OvLy+W6ut2hodCMjMaFhcKEhMITE4mVBnNgAAAAPnRSTlMAAvYS+PYL+vn39vX1zHhLBvb18OmxhFlSJRj69vTx5+bk3dbAhYJxcGhfQjwv+Pjw7dLMvo+MeGVRR0ItGzAW80wAAAIOSURBVDjLddJXc9pAFAXgK0zHdLAx7r2n9wJatBJqoIIkZFFtpyf//zlaIYIo+Z7umTM7d2dnYWbt08fbNViydXpgRXYj1sHrWwii3sZjdnc8HneNWPyUChx4GTG7qmHrtqF21d0XW+D7kIh1zfaU2Y0l3vubn+k9Hc3oPbvwGYjyUxVhD8IT6pNXpNhMqEh0tc2e2lN1MiI18Q4gmkd202WYA6tFS4YhusFG+SjcFFRS6Dr9+HX05TeNdBLVwg2cRwxN05om/1Aj7i1TdLMROYcyixRFQU22NoEUL7Nl2Mciy7JIevSbn4LuZhHvwx7WSCP88psHpu1mDe/BCStKkiSyyG96MvbyCZzFkSzLA8R994pvHay4GcXPIJtqCy6RdX6MaqN7TsIktlNZqA6xzDCMgBVuyPc77kY3yWi7CpBmNIYgN28ixZs1Jk3eLYkZ3iOz8mRicKjiNrmiJNXnSXKJAtdGvrnQiKkMENVtSaCDBGHd/0NvUspcoyQ3ph9tXWAaMwLjHyGbkoNAM0hmYCq6zvOtKYYvRuGfTEhqcRMtKXQBM9GixfsNbz3PQcBFSOY6BCeHLiEoV6KtO6JOl3Iw5zIkdBzH6Qg7VzCPOmzQ/X6fbhxSsOBqh3f6Dh/ehEXUUasxbLSOKFhSCdfv6uEKLKOOuTp3DKtkw3Q4Cyulh2lY7frPNfxHJhj+Asj8pFwOUPakAAAAAElFTkSuQmCC',
      orange : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABJlBMVEUAAAD/rRf/rhn/rBP/uDT/rxz/sSP/rBT/tjD/tCv/qxL/rBP/qxD/sCD/rRf/qxH/vUT/siT/sSD/sB3/qxL/rhj/rhf/rBL/rxz/rBL/ti7/qxD/rxz/rBP/uz7/tCn/uTf/qxH/sB//tjH/qxH/qxH/tS7/qxH/rBX/rxz/sCD/rRb/sSH/qxH/sCD/vkX/syn/tCr/qxH/qxL/syj/syj/tjD/qhD/tjD/tS7/ti//tjH/tS7/tCz/tSv/tjD/////wU3/v0r/uDT/vkf/vUP/uz7/ujr/syf/siP/wU//uTb/uzz/sB7/w1T/sSH/xlz/xVr/rhn/yGL/xFf/xl///vv/yWb/8tv/467/25j/1IX/7cv/57r/3qH/14z/0Hn/z3ZdpTAoAAAAPnRSTlMA9gP39vYT/vr374R2QxkH+fb28ulkW1RQSi4OCvn49fT05+bk3c/OycCvfXFsPPv28NfVvrSPjIiCeCcnIgRK2g4AAAH2SURBVDjLddJnl6IwFAbgC+rYexsdp/e6ve8SE0giRQRFnbL1//+JDQhHdJzn0315cw+cQ2Bpq1vsbsEz3z4fo+RBEh1/7EKc/DVTsSfz+XziVTJf5GXx4yzpThzPNmzPmTgHZ98hVMxWJq4RcSevstvhm98YUwMvGVN7vwS+D3sOJgFMFpy99yC0sm7wxHCnztQ1ghNOtgnQq2JbE2yXIV2xPC8IuNqDu9eOJhiG8u9+dv+kLM45+3dwk7QppZqrPvZ9D8gjItvJG7hiWExYY7N+ANMgsys4IhpjDFt/+wt/TENkjRxBnVC/MZ/C5jFoKKnDJdMsy9IYDpspJ0G+hHwGc84Z1n8Fxc8RoSLjzDVs1wwuaGz4e9afPegW8aNR70ApTbgpEKqP1fGIYT9wnC4BXKhU9flfTnE4qxcA0E4RdYEzHk5E8v9DOWdZaJXFGzII+aq21mi1AvhKu5apxHGeCO/QpxpdaWgqH120hBlfMs3DHQjlU1QZRMTKNUR2DlV1EDHVnFiJFFLWQF8YWFIBlno5hMIGoVwZYm4lro98OpduIa7cUNDQh5RGGVY0JXMkipEpNWGVfDJQxuOxMjiRYU1LUofjoZpowTr5VCwp+qkMz7QTaIQSbdjgrY70d7BJJ6HsdmCj8/Q5bFZMF+EFhXj4D+F6ncCPHjN+AAAAAElFTkSuQmCC',
      purple : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABOFBMVEUAAACNGo2MGIybNpuJE4mQH5CRI5GMFoyiRaKYMZiWK5aMFoyVKZWJEomJEYmKEoqKE4qIEYiKE4qTJZOIEYiIEIiIEYiIEYiIEIiXLpeXLpeJEYmfPp+JEYmOHI6PH4+YMZiJEYmXLpeIEYiKFYqOHI6QIJCYL5iNGo2ID4iRIZGTJpOXLZeIEYiQIJCIEIiHDIeVK5WXL5eUKJSUKJSYMJiIEIiXL5eVKpWUKJT///+mTKafPZ+nTqebNZulSaWUJ5SXLZejRaOYMJikR6ScOJyaM5qZMZmWK5aVKpWhQqGdOp2SI5KgP6CQH5CqVKqoUKiuXK6RIZGRI5GsWKytWq2NGo39+/2xYrGvX6+zZrOwYbDXrtfMmMy8d7yJE4nv3u/r2Ovly+Xdut3QodDGjMbChcLChMIoGGZFAAAAOnRSTlMA9gP19/UT/vr59/bz7+hTSwf59t+EeWpeLiUY+PPx5+bWz87JwK+FgnRxYERCPA4LCvq+tI+MeFEbuyLUaQAAAhBJREFUOMt10ud62jAUBuBjRtgjEEgI2XuP7iLJxmAb8MA2sZkZ3b3/O6gEJjYtff/ofDrP9+iPwLdSuaqswD82T/fERDjc3HtfgaDQx0TcnIzH46EZf3MaChQOwu7QdkzLdOyJHT7YBM9VMj50yZwzjCc/eS+nrYmFfNbITH8G5t2WjQQGEe+0t94CdZl0CcuWO7JHrsVGYicvAe6LxJQo0zVEvY0dZxpI8R5u0naDsqz289fBl98dYrFop2/gImzSSXKUpxrzKDoCzWb4Ak4MoqoqkoxBbYqoLBPjBHYFyTAMgp9rMz9li2ZJ2IUd1MAYI/mXt3niCc0NtAPHWKKTZCBvM9IElvExnCWQpmkG0b9PF996SKUZJc4gHyUyJeHuj0Ft8KhjgUUSzUM1hjSe52VB1WNKv0dfpElDsSrAEd/gGZVIuEFUNtKbIwDIcYhXpjSszSYecTm6Wc1g3FqEtWwIqFKx0VzQkqLnwFQLWBaDZC3i/aEPUbUdIKrR0vyjRWS+45P57VvwlDi1U5/rqFwZ5m63FeV1wyuZO3hV5nBdn6lj7hx8dxmx9TDTEjOrELDOaXqP0bXUOgStZkWxyzTbWVYJllJyjy56cmoDFoWy9Xa/3xfrayH4y0ZK6fa7SsSrBEtr9U6so/sVXy7S7DYjOVhiX28+7MMy+UK7kIelDl8OYbnrl2v4j3Iw/AHJ/JyS518lAwAAAABJRU5ErkJggg==',
      red : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABUFBMVEUAAAD/GRn/FBT/GBj/Fhb/IyP/Kir/JCT/HBz/Njb/EhL/FRX/EhL/Fxf/RUX/EhL/ERH/EhL/ERH/EhL/Ghr/Li7/ICD/Ghr/MDD/Pj7/LCz/ICD/NDT/Kir/HR3/Hx//ERH/ERH/ERH/Li7/ERH/FRX/HBz/ICD/EBD/Ly//Dw//ISH/ERH/HR3/EBD/ICD/EBD/DAz/MTH/Ly//Q0P/MjL/HR3/MTH/MTH/KCj/KCj/MDD/Ly//EBD/JSX/Kir/Fhb/MDD/MTH/Li7/LCz/////KCj/LCz/NDT/SUn/TEz/Tk7/Ojr/Pj7/Ly//MTH/Rkb/Nzf/JCT/IiL/PDz/QkL/Hh7/RET/XFz/VFT/UFD/Wlr/V1f/X1//Ghr/+/v/YWH/QED/Zmb/rq7/mJj/hYX/Y2P/3t7/2Nj/y8v/urr/oaH/jIz/eXn/dnb/ERH6TFh4AAAARXRSTlMAA/f2/hP39vb174JTGfr59el5SkMuCQb5+Pj29fLy5+Td1s/OycCviYV0cWxjWzwOC/r6+Pbw6OS+tI94ZltRTkInJyKnQ/irAAACAklEQVQ4y3XN1XLjMBiG4d9hbDgpM3OXmRPbkmzHFGOwsLx7/2drJe7aadPnRPrmHY0gEK99rMXhnk8HL8Sn6WXx+esahDFv5p9Yg9FodOPOPT5ggvBla9kZmK5lW645MNNbn8FXTcwNHHzLuZlbfO//vGJ3bRSwu9ZKHqhXCybiKYT901x4ScO7hIPpxk7X7Dr2+GoungDUk9hqeyxnKCqs5ro8HThZh7OESYNts3++9b9es9im00ycwVHaMgyj7XBXTepSdHlvW+kj2BOwruuoLfSbY1inGwt7sMbzgiAg7Xdz4pdse5vn12AVGbTI1365krG3DfQMdgVe0zReQH7pksnehewjRAgZYuXHOHxXke5tNJ+FD0lMPLzQ+dlv9i9VjacTr1YgH0NE9vC6EuN6qoDoICiWB9jhDI7ScVswsM5RBrcDALkU4iaIQPwbiua80shomjRNIyUGPPvJtjhFai8dApUvaDIbRkghDmP7S/pU0VNZmIgXZbkVkOXiOfiyqWGoDFNluHVe5LjgCZe5gP/KUU3xtbToIQQuMqKkTkhipg4hx1GijINCoscQ1iixYocS2VIDpryNyqoXVDlyAtOY9Rbb6/XY1joDd5xGuE6vw0VO4S5mQ2nFWsoGA/fkIlJHiuRghk1FUjZhlkqELVRgpu3YNsxW/VuFB5TD4x8uM7DaKq4DkQAAAABJRU5ErkJggg==',
      shadow : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApCAMAAACfvvDEAAAAS1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADmYDp0AAAAGXRSTlMABAkSDRcmHjY7TyxXMFtKVCNhXkZDZD8bYeT5zwAAAS5JREFUOMvt1EuShCAQRdFuEeQjhXwU9r/STtKQDEWr5h11xyce6cSfb/+836NP7tRbN7TeYXQjRvhegmN7gMneQ84Lhwgj7eTIy4QVtER7OHlR82TvKDw9CWOsNeawjV4mWRFGQmSRdhLeFlYqCC3R43ma5N6qvK45g8VZ7yfO+9GBTVatKSWwdRYpjZ4mjUpOO+eqbZQkTQrldA0pXvAgJ+mWGtK0r6K8nslMWmaoUr3TB8nV8oLQ7hegLCgrJJlfMcZqkaI0giYppl4hNAoyK2v6SWi0c2gU5HWSGoQOSMGCTPkKqeLCBhTlHaRYjtsGNlaojs8mSI1y3iqNs16l8ThI8HzognLW2YrmeoiHAozLKkXpWHdomJ0UnBG7bzBOK8/H4fMfiQvPhif1B4hkGIp6x211AAAAAElFTkSuQmCC',
      yellow : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAMAAAD3TXL8AAABIFBMVEUAAAD//xn//xP//xv//yr//zX//xD//yP//0X//xL//xP//xr//xv//xX//zD//yT//x///xL//xH//xP//xz//y7//xb//xP//z7//yD//xH//yr//xz//x///zH//xH//xH//y7//xH//xX//xz//yD//y///yH//xH//xz//xH//yD//y7//x3//xD//xD//zH//zj//xH//xL//yj//yj//zD//xD//xv//y///yX//yr//y///wz//zH//////yn//z3//zX//03//0r//zn//y7//0f//0///yP//x///0T//1X//yb//13//0H//1r//2L//xn///v//4f//2b//9v//5j//8v//7r//6///63//6H//3n//3b//xHVeFJ6AAAAPnRSTlMA9vcC9/Z3E/rvhFEJ/fn29ulXS0IlBvn49PTy8efm5N3PzsnAr4VxbGVfPC4aFw368tfVvrSPjIB4W0ctDx1XNmoAAAHvSURBVDjLddLnduIwEAXgsQ0ECEloIb33vr2vQdVyQS5Asn3f/y0WCbOYku/XXF/Psc+RYGLpS/N2Ceasn+/z/Ms83393C1nWx9ViEA8Ggzgorp5bmYWjfBTLIBBBIGOZP1qH1OdcMY7EWBQXc8vpl3dELMiEkMHOHShvNyWhGqEjcvONKj7lIv1ERFLKSOg3ZO4KoLJGAmcoiELe5SgYBbJWgZucVLMQ/M9j77HfIUJFmbuBi3zg+74T4ae28pNHdJiD/AWcIRKGIXFQr62RUGd0BnvUQQgR9Ls90sdimB26B7vUVw3rp82Tbny6C6dI7TiIpI1kVOdTKL8gjLGQuN918c2j/jCT1Q+wvCXYkIO8X71274eLqIpiqwF3K5RhjBkN3cROPER0Iiv3ACfYx4r6c5+MZh+fAEDdoNhWMEMsnaihzqFVQsiehlhNn2t5zZlpnGoBlPsNxHgWY+YSaO+rPu9McL9aHl80E+NMg/H2V0iVjTDThEYBxh62Me6OYbv0AP8VDNR1R7pIr4xVStxOG5uXKpBxaTDXU1xmXEJWq8a5bmxea8GUK4PphpnXMM066PAkSXjnwIIZ1yb2Eg+bdZhlHXY7Sad7aMGcuml7trkM86xXru2+hkUaJt9owELHK8ewWPNvE55RyIZ/d+ycl8ybNvoAAAAASUVORK5CYII=',
    }
    var icone = new L.Icon({
      shadowUrl: images["shadow"],
      iconUrl: images[couleur],
      iconSize: [25,41],
      iconAnchor: [12, 41],
      shadowSize:   [41, 41],
      shadowAnchor: [12, 41]
    });
    return icone;
  };
//
// ajout du marqueur
  function pointToLayer(feature, latlng) {
    var options = {
      feature: feature,
      icon : getIcone("green"),
      riseOnHover : true,
      draggable : false,
      clickable : true,
    };
    if (feature.properties && feature.properties.espece) {
       return L.marker(latlng, options);
    };
  }
  function zoomToFeature(e) {
    map.setView(e.latlng, 17);
  }
//
// une fenêtre popup
  function onEachFeature(feature, layer) {
    if (feature.properties && feature.properties.espece) {
      layer.bindPopup(feature.properties.espece);
      layer.on({
        click: zoomToFeature
      });
      tr = '<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '">';
      tr = tr + '<td class="feature-name">' + layer.feature.properties.espece + '</td>';
      tr = tr + '<td class="feature-name">' + layer.feature.properties.date + '</td>';
      tr = tr + '<td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td>';
      tr = tr + '</tr>';
      $("#feature-list tbody").append(tr);

    }
  }
 $("#feature-list tbody").empty();
  var layer = L.geoJSON(null, {
    pointToLayer: pointToLayer,
    onEachFeature: onEachFeature
  });
  $.getJSON("gvu.php?action=get_obs", function (data) {
    layer.addData(data);
    map.addLayer(layer);
  });
  if (obs_layer) {
    map.removeLayer(obs_layer);
    controlLayers.removeLayer(obs_layer);
  }
  obs_layer = layer;
  if (obs_layer) {
    obs_layer.addTo(map);
    name = "obs";
    controlLayers.addOverlay(obs_layer, name);
  }
  return layer;
}
//
// la page est prête,
window.onload = function() {
  console.log("window.onload()")
// pour l'api Géoportail
  var apiKEYs = {
    'localhost': '9m2r220489kkm1x2s7lq6wdh',
    'bv': '3os2g9rimugnfxc2nlym5xce',
    'www.bretagne-vivante.org': 'uit9cf7rn73kf8qevrgjemfd',
    'www.bretagne-vivante-dev.org': 'uu5fmm020wh4jcm7blyrsn0u',
    'bretagne-vivante-dev.org': 'uu5fmm020wh4jcm7blyrsn0u',
    'ao35.free.fr': '61y24ij6lwe2wpro30f2ge45',
    'atlasnw.free.fr': 'dgdc5nlbj5apwtmuk4wp4ecn',
    'mga.alwaysdata.net': 'wv7w208rgqxf81bp4chcfmx0'
  };
  conf.apiKEY = apiKEYs[window.location.host];
  map_init();
  map_position();
  form_init()
  obs_init();
}
//
// un clic sur une feature de la marge
$(document).on("click", ".feature-row", function(e) {
  sidebarClick(parseInt($(this).attr("id"), 10));
});
function sidebarClick(id) {
  var layer = obs_layer.getLayer(id);
  map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 16);
  layer.fire("click");
}

$("#about-btn").click(function() {
  $("#aboutModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});
$("#especes-btn").click(function() {
  $("#especesModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});
$("#ajout-btn").click(function() {
  $("#liste").hide();
  $("#ajout").show();
  map_position(true);
  return false;
});
$("#liste-btn").click(function() {
  $("#ajout").hide();
  $("#liste").show();
  map_position(false);
  return false;
});
//
// pour l'horodatage, initialisation de la zone à l'instant présent
horo=moment().format('DD-MM-YYYY HH:mm');
console.log("horodatage ", horo);
$('.date-fr').val(horo);
$('.date-fr').combodate({
  minYear: 2017,
  maxYear: 2018,
  minuteStep: 10
});
//
// mise en place des formulaires
function form_init() {
  form_contact();
  form_obs();
};
//
// création dynamique des espèces dans le formulaire
function form_obs() {
  var $html = $('.espece_template').clone();
  $('#especesModal img').each(function() {
    console.log($(this).attr('alt'))
    var html = $html.html();
    html = html.replace(/Option/g, $(this).attr('alt'));
    $(".especes" ).append(html);
  });
  $('#form_obs').on('submit', function(e) {
    e.preventDefault();
    console.log("formm_obs_() submit");
    var $this = $(this);
//    alert($(this).serialize());
    $.ajax({
      url: 'gvu.php?action=sauve_obs',
      type: 'POST',
      data: $this.serialize(),
      dataType: 'json', // JSON
      success: function(json) {
        if(json.rc === 0 ) {
          message = "<p>Donnée enregistrée, merci de votre participation</p>";
        } else {
          message = json.message;
        }
        $("#messageBody").html(message);
        $("#messageModal").modal("show");
        obs_init();
      }
    });
  });
};
//
// fomulaire de contact
function form_contact() {
  $('#form_contact').on('submit', function(e) {
    e.preventDefault();
    console.log("formm_contact_() submit");
    var $this = $(this);
    $.ajax({
      url: 'gvu.php?action=sauve_obs',
      type: 'POST',
      data: $this.serialize(),
      dataType: 'json', // JSON
      success: function(json) {
        if(json.rc === 0 ) {
          message = "<p>Message envoyé, merci de votre participation</p>";
        } else {
          message = json.message;
        }
        $("#messageBody").html(message);
        $("#messageModal").modal("show");
      }
    });
  });
};