<?php
// auteur : Marc Gauthier
// licence : http://creativecommons.org/licenses/by-nc-sa/2.0/fr/ licence Creative Commons Paternité-Pas d'Utilisation Commerciale-Partage des Conditions Initiales à l'Identique 2.0 France
//
require_once ('../inc/gvu.php');
//
// les traitements bruts
//
// production d'un fichier geojson de toutes les obs
//  php gvu.php GET action=get_obs
if (isset($_GET['action']) && $_GET['action'] === 'get_obs' ) {
  get_obs();
  exit;
}
// sauvegarde d'une observation
//  php gvu.php POST action=sauve_obs email='mga_geo@yahoo.fr' nom='Gauthier' prenom='Marc' espece='toto' message='' latitude='48' longitude='-1'
if (isset($_REQUEST['action']) && $_REQUEST['action'] === 'sauve_obs' ) {
  $retour = sauve_obs();
  echo json_encode($retour);
  exit;
}
//
if (isset($_REQUEST['action']) && $_REQUEST['action'] === 'contact' ) {
  $retour = contact();
  echo json_encode($retour);
  exit;
}
// die("BUG absent action");
?>
<!DOCTYPE html>
<html lang="<?php echo $lang ?>">
<head>
</head>
<body>
<h1>Rennes gvu</h1>
</body>
</html>